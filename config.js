module.exports = {
    'name'  : 'Hero 04',
    'camel' : 'Hero04',
    'slug'  : 'hero4',
    'dob'   : 'hero_4_1440',
    'desc'  : 'A basic hero banner with title, text, cta & an image',
}
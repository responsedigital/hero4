<?php

namespace Fir\Pinecones\Hero04;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Hero04',
            'label' => 'Pinecone: Hero 04',
            'sub_fields' => [
                [
                    'label' => 'Overview',
                    'name' => 'overviewTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A basic hero banner with title, text, cta & an image"
                ],
                [
                    'label' => 'Text & CTA',
                    'name' => 'contentTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text',
                    'required' => 1
                ],
                [
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'textarea',
                    'required' => 1
                ],
                [
                    'label' => 'CTA',
                    'name' => 'cta',
                    'type' => 'link'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                    GlobalFields::getOptions(array(
                        //'backgroundColor',
                    )) ,
                    [
                        'label' => '',
                        'name' => 'options',
                        'type' => 'group',
                        'layout' => 'row',
                        'sub_fields' => [
                            GlobalFields::getOptions(array(
                            'hideComponent',
                            'flipHorizontal',
                        ))         
                    ]
                ]          
            ]
        ];
    }
}
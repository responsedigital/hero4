<!-- Start Hero 04 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A basic hero banner with title, text, cta & an image -->
@endif
<div class="hero4 {{ $classes }}" is="fir-hero4">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="hero4__wrap">
    <div class="hero4__col hero4__col--copy">
        <h2 class="hero4__title">
        {{ Fir\Utils\Helpers::checkMeta($meta, 
                                        $title, 
                                        'Lorem ipsum dolor sit amet.') 
        }}
        </h2>

        <p class="hero4__text">
            {{ Fir\Utils\Helpers::checkMeta($meta, 
                                            $text, 
                                            'Quidam officiis similique sea ei, vel tollit indoctum efficiendi ei, at nihil tantas platonem eos.') 
            }}
        </p>

        <a href="/" class="btn btn--blue">Sign Up</a>
    </div>
    <div class="hero4__col hero4__col--img">
        <img src="https://res.cloudinary.com/fir-design/image/upload/f_auto,q_auto/Pinecones/phone_hs9xxy.png" class="hero4__img" alt="Test Image">
    </div>

    <svg class="hero4__down" data-scroller viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Hero" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Hero_4_1440" transform="translate(-135.000000, -696.000000)">
                <rect id="Rectangle" fill-rule="nonzero" x="0" y="0" width="1440" height="768"></rect>
                <polygon id="Path" fill="#0076FF" transform="translate(146.000000, 708.000000) rotate(90.000000) translate(-146.000000, -708.000000) " points="147.025 697 144.178 699.828 150.354 706.004 134 706.004 134 709.996 150.354 709.996 144.178 716.172 147.025 719 158 708"></polygon>
            </g>
        </g>
    </svg>
  </div>
</div>
<!-- End Hero 04 -->
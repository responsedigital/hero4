class Hero04 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {
        this.scroller = this.querySelector('[data-scroller]');
        this.parent = this.scroller.closest('.hero04')
    }

    connectedCallback () {
        this.initHero04()
    }

    initHero04 () {
        const { options } = this.props
        const config = {

        }


        this.scroller.addEventListener('click', e => {
            const bottom = this.scroller.getBoundingClientRect().top + window.scrollY;
            window.scrollTo({
                top: bottom - 100,
                left: 0,
                behavior: 'smooth'
            })
        })
    }

}

window.customElements.define('fir-hero4', Hero04, { extends: 'div' })
